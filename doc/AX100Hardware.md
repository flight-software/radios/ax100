# How to Run the AX100's in the Lab

### General Handling
- Always wear clean gloves to prevent oils from getting on the radio
- Always wear an antistatic wrist strap when handling the radio
- The last thing to disconnect from the radio and the first thing to connect should always be the attenuator/dummy load
- Never use the debug interface at the same time as the flight serial interface unless you know what you're doing

## Part 1: The Debug Console
- Running the radio with the debug console is the easiest way to verify the radio functionality, and **cannot use an external power supply**
- Plug in the attenuator/dummy load found in the pelican case into the AX100
- Place the AX100 (with or without support board) on an anti-static mat and then plug in the small white connector from the included usb-serial adapter into the radio
- Using either a desktop computer, a laptop running on battery, or a laptop with a 3-prong charger, plug in the usb adapter (anything running off of 120V needs a ground plug to prevent weird coupling issues)
- Open the serial port in putty at 500k baud and 1 stop bit, zero parity. I have not been able to run minicom/picocom successfully at this nonstandard baudrate yet

## Part 2: Using the Flight Configuration
- Plug in the attenuator/dummy load found in the pelican case into the AX100
- Plug the two custom harnesses (located in the pelican case) into the AX100 support board (please have Radhay confirm the pinout against the datasheet and the PCB design files before doing this)
- Attach the power harness to a DC power supply set to 3v3 and a current limit of 1.0 Amps with the output turned off
- Attach the TX, RX and GND lines to a usb-serial adapter (do not attach 3v3), but leave the usb unplugged for now
- Turn of the DC supply and verify the current draw is less than 100 mA (approx 70 mA is normal I think)
- Plug in the usb and the open the serial port as normal
- When done, unplug the usb before powering off the power supply output
