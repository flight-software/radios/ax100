#define AX100_OP_UNKNOWN        0x0
#define AX100_OP_SCHEDULE       0x1
#define AX100_OP_DOWNLINK       0x2
#define AX100_OP_UPLINK         0x3
#define AX100_OP_SHELLEX        0x4
#define AX100_OP_BEACON         0x5
#define AX100_OP_MAX            0x5

//The lowest value the leading byte of an IV for downlink can have. Must be odd (even values are used for uplink) and higher than
//AX100_OP_MAX (otherwise the ground could confuse encrypted packets with unencrypted command grammar packets. We are using 7 here, larger values would reduce the number of valid iv's.
#define AX100_LOWEST_IV 0x07U

#define AX100_ERR_UNKNOWN       0x1
#define AX100_ERR_HWCLOCK       0x2
#define AX100_ERR_SWCLOCK       0x3
#define AX100_ERR_NULLTERM      0x4
#define AX100_ERR_SHELLCMD      0x5

#define AX100_ENC_SIZE 4

#if !defined(AX100_ENC_KEY0) || !defined(AX100_ENC_KEY1) || !defined(AX100_ENC_KEY2) || !defined(AX100_ENC_KEY3)
#warning "The key should be defined at build time to improve security"
#ifndef AX100_ENC_KEY0
#define AX100_ENC_KEY0 0x00
#endif

#ifndef AX100_ENC_KEY1
#define AX100_ENC_KEY1 0x00
#endif

#ifndef AX100_ENC_KEY2
#define AX100_ENC_KEY2 0x00
#endif

#ifndef AX100_ENC_KEY3
#define AX100_ENC_KEY3 0x00
#endif
#endif
