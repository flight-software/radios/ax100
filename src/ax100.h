#ifndef AX100_H
#define AX100_H

#pragma weak csp_iflist_add

#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>

#include <csp/drivers/usart.h>
#include <csp/arch/csp_thread.h>

#include "param/rparam_client.h"

//TODO: add actual filepath for file downloads
#define LIBRFT_PREFIX ~/.cache/librft/partial //default path

#ifndef HMAC_KEY_LENGTH
#define HMAC_KEY_LENGTH 16
#endif

extern int ax100_init(const char *path);
extern int ax100_loop();
extern int ax100_close();

extern int ax100_payload_tx(uint8_t *data, size_t  size);
extern int ax100_payload_rx(uint8_t *data, size_t *size);

extern int ax100TxTest(uint8_t * data, size_t size);
extern int ax100RxTest(uint8_t * data, size_t size);
//extern void csp_route_work(uint8_t value);

// CSP port numbers
// Most of what we do would be 7 and 9

// Ports 0 through 6 look to be standard for all csp, so some csp functions
// may just assume they are the values stated here

#define AX100_CSP_CMP 0
#define AX100_CSP_PING 1
#define AX100_CSP_PS 2
#define AX100_CSP_MEMFREE 3
#define AX100_CSP_REBOOT 4
#define AX100_CSP_BUF_FREE 5
#define AX100_CSP_UPTIME 6

#define AX100_PORT_RPARAM 7
#define AX100_PORT_GNDWDT_RESET 9

// NOTE: float == 32 bits long, should probably force this somehow

/**
 * \brief AX100 System Configuration
 */
typedef struct __attribute__((packed)){
    int8_t rssi_offset;
    int16_t max_temp;
    float bgndrssi_ema;
    int8_t i2c_en;
    int8_t can_en;
    int8_t extptt_en;
    int8_t led_en;
    int8_t kiss_usart;
    int8_t gosh_usart;
    uint8_t i2c_addr;
    uint16_t i2c_khz;
    uint16_t can_khz;
    uint16_t reboot_in;
    uint32_t tx_inhibit;
    uint8_t log_store;
    uint8_t tx_pwr;
    uint32_t bcn_interval;
    uint32_t bcn_holdoff;
    uint32_t max_tx_time;
    uint32_t max_idle_time;
    char csp_rtable[];
} ax100_config_system_t;

/**
 * \brief AX100 Receiver Configuration
 */
typedef struct __attribute__((packed)){
    uint32_t freq;
    uint32_t baud;
    float modindex;
    uint16_t guard;
    uint8_t pllrang;
    uint8_t mode;
    uint8_t csp_hmac;
    uint8_t csp_rs;
    uint8_t csp_crc;
    uint8_t csp_rand;
    uint8_t csp_hmac_key[HMAC_KEY_LENGTH];
    char ax25_call[7]; // callsign of the satellite itself
    uint32_t bw;
    int32_t afcrange;
} ax100_config_receiver_t;

/**
 * \brief AX100 Transmitter Configuration
 */
typedef struct __attribute__((packed)){
    uint32_t freq;
    uint32_t baud;
    float modindex;
    uint16_t guard;
    uint8_t pllrang;
    uint8_t mode;
    uint8_t csp_hmac;
    uint8_t csp_rs;
    uint8_t csp_crc;
    uint8_t csp_rand;
    uint8_t csp_hmac_key[16];
    char ax25_call[7]; // dest callsign (groundstation)
    uint8_t preamb;
    uint8_t preamblen;
    uint8_t intfrm;
    uint8_t intfrmlen;
    int16_t rssibusy;
    uint8_t kup_delay;
    uint16_t pa_level; // don't modify apparently
    float ber; // lol, this injects bit errors into frames, but can it inject sync bytes?
} ax100_config_transmitter_t;

/**
 * \brief AX100 Telemetry Configuration
 */
typedef struct __attribute__((packed)){
    int16_t temp_brd;
    int16_t temp_pa;
    int16_t last_rssi;
    int16_t last_rferr;
    uint32_t tx_count;
    uint32_t rx_count;
    uint32_t tx_bytes;
    uint32_t rx_bytes;
    uint8_t active_conf;
    uint16_t boot_count;
    uint32_t boot_cause;
    uint32_t last_contact;
    int16_t bgnd_rssi;
    uint8_t tx_duty;
    uint32_t tot_tx_count;
    uint32_t tot_rx_count;
    uint32_t tot_tx_bytes;
    uint32_t tox_rx_bytes;
} ax100_telemetry_t;

#endif
