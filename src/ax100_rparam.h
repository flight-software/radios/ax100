#ifdef AX100_RPARAM
#include <param/rparam_client.h>
#define RADIO_ADDR 5
#define AX100_PORT_RPARAM 7

void rparam_debug_print_u32(uint16_t addr, int table_num);
void rparam_debug_print_i8(uint16_t addr, int table_num);
void rparam_debug_print_u8(uint16_t addr, int table_num);
void rparam_debug_set_u8(uint8_t val, uint16_t addr, int table_num);
void rparam_debug_set_u32(uint32_t val, uint16_t addr, int table_num);

void rparam_debug_print_u32(uint16_t addr, int table_num) {
   unsigned buf = 0;
   int size = rparam_get_uint32(&buf, addr, table_num, RADIO_ADDR, AX100_PORT_RPARAM, 200);
   if (size > 0) {
       P_INFO("parameter value: %u", buf);
   }
   else {
       P_ERR_STR("failed to access rparam parameter");
   }
}

void rparam_debug_print_u8(uint16_t addr, int table_num) {
   uint8_t buf = 0;
   int size = rparam_get_uint8(&buf, addr, table_num, RADIO_ADDR, AX100_PORT_RPARAM, 200);
   if (size > 0) {
       P_INFO("parameter value: %d", buf);
   }
   else {
       P_ERR_STR("failed to access rparam parameter");
   }
}

void rparam_debug_print_i8(uint16_t addr, int table_num) {
   int8_t buf = 0;
   int size = rparam_get_int8(&buf, addr, table_num, RADIO_ADDR, AX100_PORT_RPARAM, 200);
   if (size > 0) {
       P_INFO("parameter value: %d", buf);
   }
   else {
       P_ERR_STR("failed to access rparam parameter");
   }
}

void rparam_debug_set_u8(uint8_t val, uint16_t addr, int table_num) {
   int size = rparam_set_uint8(&val, addr, table_num, RADIO_ADDR, AX100_PORT_RPARAM, 200);
   if (size > 0) {
       P_INFO("set parameter to %u", val);
   }
   else {
       P_ERR_STR("failed to access rparam parameter");
   }
}

void rparam_debug_set_u32(uint32_t val, uint16_t addr, int table_num) {
   int size = rparam_set_uint32(&val, addr, table_num, RADIO_ADDR, AX100_PORT_RPARAM, 200);
   if (size > 0) {
       P_INFO("set parameter to %u", val);
   }
   else {
       P_ERR_STR("failed to access rparam parameter");
   }
}

static inline int rparam_get_boolean(uint8_t * out, uint16_t addr, int index_id, int node, int port, int timeout) {
    return rparam_get_single(out, addr, PARAM_BOOL, sizeof(uint8_t), index_id, node, port, timeout);
}

static inline int rparam_set_boolean(uint8_t * in, uint16_t addr, int index_id, int node, int port, int timeout) {
    return rparam_set_single(in, addr, PARAM_BOOL, sizeof(uint8_t), index_id, node, port, timeout);
}
#endif
