#include <csp/csp.h>
#include <csp/interfaces/csp_if_kiss.h>
#include <csp/drivers/usart.h>
#include <time.h>
#include <sys/time.h>

//At least for the CAPSat mission, we will be using 9600 baud since the link budget is based on this figure.
#define AX100_BAUD 9600

//Used to wait after sending a packet. This is necessary to ensure correct timeout behaviour since UDP packets are sent at a much higher rate than the AX100 baud rate,
////so there will be a delay between the time send() returns and when the packet is actually sent.
#ifndef AX100_BAUD
#warning "No AX100 baud defined, defaulting to 9600bps. Needs to be updated if this changes to prevent issues with large files or decreased throughput"
#define AX100_BAUD 9600
#endif

csp_packet_t* recv_message(csp_socket_t* sock, struct timeval* timeout);

//These defines need to be coordinated with the hm-d implementation. Make sure they are up to date
#define RPC_BEACON_WAIT (1000000) // 1 second
#define HMD_RPC_SOCKET UINT16_C (50100)
#define HMD_RPC_OP_GET_BEACON (0x02)
#define AX100_BEACON_LENGTH 74

#ifdef UDP_COMMS
//Appropriating GNURadio's port here
#define GR_PORT_STR "1234"
#endif
