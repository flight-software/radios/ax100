/*
Cubesat Space Protocol - A small network-layer protocol designed for Cubesats
Copyright (C) 2012 GomSpace ApS (http://www.gomspace.com)
Copyright (C) 2012 AAUSAT3 Project (http://aausat3.space.aau.dk)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdint.h>
#include <sys/stat.h>
#include <signal.h>
#include <time.h>           /* for setting software clock */
#include <errno.h>

#include <libcrap.h>
#include <libencrypt.h>
#include <librpc.h>
#include <libsll.h>
#include "libdaemon.h"
#include "libdebug.h"       /* error logging */
#include "powerboard.h"     /* Defines to power cycle AX100*/
#include "ax100_io.h"
#include "ax100_rparam.h"
#include "cmd_grammar.h"

//Using ASM + Golay format (mode 5)

//CSP addresses and ports
#define AX100_ADDR 5
#define GROUND_ADDR 14
#define GROUND_PORT 14
#define CSP_CDH_PORT 1
#define CSP_CDH_ADDR 8

//Send this many seconds after being idle, except for HMD_BEACON_SEND_FREQ_TRANSMISSION
#define HMD_BEACON_SEND_FREQ_IDLE 30
//Send this many seconds after receiving or sending files/scripts/requests (i.e. things other than beacons)
#define HMD_BEACON_SEND_FREQ_TRANSMISSION 300
//Locations of AX100 configuration and telemetry parameters
#define AX100_TELEM_TABLE_NO 4
#define AX100_LED_ENABLE 0x0D
#define AX100_FCS 0x3B
#define AX100_TELEM_BOOT_COUNT 0x20
//CSP configuration parameters

//Kiss and Gomspace configuration parameters
#define CSP_NUM_BUFFERS 400 //Smaller value should still be ok
#define PACKET_BUF_SIZE 256 //Apparently not the same as MTU. This value works.

//43 bytes for sync word, golay field, csp header and reed-solomon. See gr-ax100 code for details.
#define AX100_WRAP_SIZE 43

#define AX100_IV_FILE "/var/log/cdh/rgd_iv"

int csp_crap_send2(const uint8_t* crap_packet, size_t crap_size, struct timeval* timeout, unsigned char is_encrypted);
int csp_crap_send(const uint8_t* crap_packet, size_t crap_size, struct timeval* timeout);
int csp_crap_recv(uint8_t* crap_packet, size_t* crap_size, struct timeval* timeout);
void csp_crap_received_file(char* file);
int ax100_check_alive(void);

#ifdef UDP_COMMS
int recv_message_udp(unsigned char* packet, struct timeval* timeout);
#endif

#define AX100_RPC
static unsigned char* get_beacon(unsigned char beacon_retry){
#ifdef AX100_RPC
  unsigned char* beacon = calloc(AX100_BEACON_LENGTH, 1);
  if(!beacon) {
    P_ERR_STR("Failed to allocate space for beacon");
    return beacon;
  }

  const int max_beacon_receive_attempts = beacon_retry ? 5 : 1;
  int beacon_receive_attempts = 0;
  while (beacon_receive_attempts < max_beacon_receive_attempts) {
    if (rpc_recv_buf(HMD_RPC_SOCKET, HMD_RPC_OP_GET_BEACON, beacon, AX100_BEACON_LENGTH, RPC_BEACON_WAIT) != EXIT_SUCCESS) {
      //Deliberately send a zero beacon if this fails repeatedly, this will allow some diagnostics on the ground, which is better than not sending beacons.
      P_ERR_STR("Failed to receive beacon from hm-d");
      beacon_receive_attempts++;
      continue;
    }
    return beacon;
  }
  return beacon;
#else
  return malloc(AX100_BEACON_LENGTH);
#endif
}

static void ax100_send_error(uint8_t opcode, uint32_t error_code) {
  uint32_t error_offset = 0;
  //Send opcode and beacon for AX100_OP_BEACON, otherwise also send error code, errno
  if (opcode != AX100_OP_BEACON) {
      error_offset = 8;
  }
  uint32_t buf_size = 1 + error_offset + AX100_BEACON_LENGTH;
  //Use calloc so we send an all-zero beaon in case we can't get a beacon response
  unsigned char* buffer = (unsigned char*)calloc(1, buf_size);
  buffer[0] = opcode;
  if (opcode != AX100_OP_BEACON) {
      memcpy(buffer + 1, &error_code, 4);
      memcpy(buffer + 5, &errno, 4);
  }

  //If a beacon was requested from the ground or this is a scheduled beacon repeatedly try to get the beacon response.
  unsigned char* beacon = get_beacon(opcode == AX100_OP_BEACON);
  if (beacon) {
    memcpy(buffer + 1 + error_offset, beacon, AX100_BEACON_LENGTH);
    free(beacon);
  }

  struct timeval t;
  t.tv_sec = 5;
  t.tv_usec = 0;
  //Both beacons and error resposes containing beacons will be unencrypted, the only additional information exposed is error code, current time and errno.
  csp_crap_send2(buffer, buf_size, &t, 0);
  free(buffer);
}

csp_socket_t* globalsock = 0;
sll_fs commands;
time_t next_beacon_time = 0;

/* Process packet */
static int process_packet(unsigned char * packet_data, unsigned length) {
    //for more info on csp_packet_t see csp_types.h
    //see the ax100 com spec document for packet handling info

    if(packet_data != NULL && length >= 4 && memcmp("CRAP", packet_data, 4) == 0) {
        //Hand off to libcrap
        return 1;
    }

    if (packet_data == NULL || length < 6) {
        P_ERR_STR("Received a malformed packet");
        ax100_send_error(AX100_OP_UNKNOWN, AX100_ERR_UNKNOWN);
        return 0;
    }

    if (packet_data[length - 1] != '\0') {
        P_ERR_STR("Received a malformed packet");
        ax100_send_error(AX100_OP_UNKNOWN, AX100_ERR_NULLTERM);
        return 0;
    }

    uint8_t     opcode;                     //size: 1 byte
    uint32_t    timestamp;                  //size: 4 bytes. time_t may be larger, but we use 4 bytes in the packet.

    opcode    = (uint8_t) packet_data[0];
    memcpy(&timestamp, packet_data + 1, sizeof(uint32_t));
    if(opcode > AX100_OP_MAX || opcode == AX100_OP_UNKNOWN) {
        P_ERR_STR("Received packet with unknown opcode");
        ax100_send_error(opcode, AX100_ERR_UNKNOWN);
        return 0;
    }

    //first index after opcode and timestamp
    uint16_t OFFSET = 5;

    unsigned char* data = packet_data + OFFSET;

    //Will be disabled when just using UDP so we don't accidentially set the clock.
#ifndef UDP_COMMS
    //every time we get a packet we want to set the clock
    time_t packet_time = (time_t)timestamp;
    time_t sat_time = time(0);
#define TIME_MAX_DRIFT 10
    int time_diff = abs(packet_time - sat_time);
    if (time_diff > TIME_MAX_DRIFT) {
        if (stime(&packet_time) != 0) {
            //Log error: could not set software clock
            //However, will probably want to still process the packet.
            P_ERR_STR("Failed to set sw clock");
            ax100_send_error(opcode, AX100_ERR_SWCLOCK);
        }

        int sys_error = system("hwclock --systohc");
        if (sys_error != 0) {
            //Log error: could not set hardware clock
            //However, will probably want to still process the packet.
            P_ERR("Failed to set hw clock, error code %d", sys_error);
            ax100_send_error(opcode, AX100_ERR_HWCLOCK);
        }

        //Now that the system time has changed, we also need to update this so we don't wait too long/too short to send the new beacon
        next_beacon_time = time(0) + HMD_BEACON_SEND_FREQ_TRANSMISSION;
    }
#endif

    //handle the info we got according to the spec
    switch(opcode) {
    case 0x01:; {
        //upload schedule, str is filename
        return 0;
    }
    break;

    case 0x02:; {
        //downlink file, str is filename
        P_INFO_STR("Got a downlink request");
        char* filename = strdup((char*)data);
        if(sll_push_head(&commands, filename, strnlen(filename, CRAP_MAX_PATH_LEN), SLL_NOWRITE) != EXIT_SUCCESS){
            P_ERR_STR("Could not add file name to sll");
        }
        return 2;
    }
    break;

    case 0x03:; {
        //uplink file, str is filename
        return 0;
    }
    break;

    case 0x04:; {
        //shell execution, str is shell command
        P_INFO_STR("Executing script");
        //This name should be unique since schedule files get deleted after execution.
        //This approach won't allow us to get and send an error code for the command.
        FILE* script_file = fopen("temp.sched", "wb");
        if(!script_file || fwrite(data, 1, length - OFFSET - 1, script_file) != length - OFFSET - 1 || fclose(script_file) != 0){
            P_ERR_STR("Failed to write script file");
            ax100_send_error(opcode, AX100_ERR_SHELLCMD);
        }
        else {
          csp_crap_received_file("./temp.sched");
        }
        return 0;

    }
    break;
    case 0x05:; {
        P_INFO_STR("Requested beacon");
        ax100_send_error(opcode, AX100_OP_BEACON);
        return 0;
    }
    break;
    default:; {
        P_ERR_STR("Unknown opcode");
        ax100_send_error(opcode, AX100_ERR_UNKNOWN);
        return 0;

    }
    break;
    }
    return 0;
}

static int init_route(char* device){
    //This should be the correct baud rate
    uint32_t baud = 500000;
    /**
     * KISS interface
    */
    static csp_iface_t csp_if_kiss;
    static csp_kiss_handle_t csp_kiss_driver;
    static const char * kiss_name = "KISS";
    if(csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_kiss, CSP_NODE_MAC) != CSP_ERR_NONE){
      P_ERR_STR("Failed to set route");
      return 1;
    };
    csp_route_set(AX100_ADDR, &csp_if_kiss, CSP_NODE_MAC);
    csp_route_set(GROUND_ADDR, &csp_if_kiss, CSP_NODE_MAC);

    csp_kiss_init(&csp_if_kiss, &csp_kiss_driver, usart_putc, usart_insert, kiss_name);
    struct usart_conf conf = {.device = device, .baudrate = baud};
    conf.databits = 8;
    conf.stopbits = 1;
    conf.paritysetting = 0;
    conf.checkparity = 0;

    usart_init(&conf);
    void my_usart_rx(uint8_t * buf, int len, void * pxTaskWoken) {
      csp_kiss_rx(&csp_if_kiss, buf, len, pxTaskWoken);
    }
    usart_set_callback(my_usart_rx);
    return 0;
}

int ax100_check_alive(void) {
   int ping_time = csp_ping(AX100_ADDR, 1000, 4, CSP_O_NONE);
   if (ping_time < 0) {
       P_ERR_STR("Failed to ping radio, radio or connection is dead");
       return EXIT_FAILURE;
   }
   else {
#ifdef DEBUG
       P_INFO("Successfully pinged radio [%d ms]", ping_time);
#endif
#ifdef AX100_RPARAM
#error For the CAPSAT mission, the configuration is finalized, no more changes should be necessary.
#error Running these can be outright dangerous if parameters are changed and not changed back, e.g. tx_inhibit
       rparam_debug_print_u32(0x1C, 0);
       rparam_debug_set_u32(0, 0x1C, 0);
       rparam_debug_print_u32(0x1C, 0);
#endif
       return EXIT_SUCCESS;
   }
}

static csp_socket_t* csp_initalize_socket(char* device){
    /* Init CSP and CSP buffer system */
    if (csp_init(CSP_CDH_ADDR) != CSP_ERR_NONE || csp_buffer_init(CSP_NUM_BUFFERS, PACKET_BUF_SIZE) != CSP_ERR_NONE) {
        P_ERR_STR("Failed to init CSP");
        return 0;
    }
    
    if(init_route(device) != 0){
      return 0;
    }
    
    //Start router. Parameters are ignored on linux.
    if(csp_route_start_task(1000, 0) != CSP_ERR_NONE){
      P_ERR_STR("Failed to start route task");
      return 0;
    }
    P_INFO_STR("CSP route started");

    /* Create socket and listen for incoming connections on all ports */
    csp_socket_t *sock = csp_socket(CSP_SO_NONE);
    if(!sock){
      P_ERR_STR("Could not create csp socket");
      return 0;
    }
    csp_bind(sock, CSP_ANY);
  
    //length queue of 10 should be enough. This is believed to be the number of connections
    //we can have at a time, and we use only one.
    csp_listen(sock, 10);
    return sock;
}

int csp_crap_recv(uint8_t* crap_packet, size_t* crap_size, struct timeval* timeout){
    //To avoid a multithreading interface, have a singly linked list that contains all files that are to be downlinked. The radio daemon is either in client or server mode at a given time and switches based on transmissions ending and new files to be transmitted coming in via command grammar from the ground. In either mode, command grammar operations can be done and single-packet responses will be sent. We need code to shut down a libcrap client session if there is no ongoing transmission and we need to downlink a file, make sure that all memory is freed.
    while(true){
#ifdef UDP_COMMS
    csp_packet_t* packet = csp_buffer_get(PACKET_BUF_SIZE);
    unsigned char* buf = malloc(PACKET_BUF_SIZE);
    int packet_size = recv_message_udp(buf, timeout);
    if(packet_size <= 0) {
        csp_buffer_free(packet);
        packet = 0;
    }
    else {
        packet->length = (unsigned short)packet_size;
        memcpy(packet->data, buf, (size_t)packet_size);
    }
    free(buf);
#else
    csp_packet_t* packet = recv_message(globalsock, timeout);
#endif
    if(packet) {
        if(packet->length > *crap_size + AX100_ENC_SIZE){
          P_ERR("Received %dB packet, but only %zuB allocated", packet->length - AX100_ENC_SIZE, *crap_size);
          csp_buffer_free(packet);
          continue;
        }
        P_INFO("Received %d bytes", packet->length);
        next_beacon_time = time(0) + HMD_BEACON_SEND_FREQ_TRANSMISSION;

        uint32_t iv[4] = {0};
        uint64_t key[4] = {0};
        key[0] = AX100_ENC_KEY0;
        key[1] = AX100_ENC_KEY1;
        key[2] = AX100_ENC_KEY2;
        key[3] = AX100_ENC_KEY3;
        memcpy(iv, packet->data, AX100_ENC_SIZE);
        unsigned decrypted_size = (unsigned)packet->length - AX100_ENC_SIZE;
        unsigned char* decrypted = (unsigned char*)malloc(decrypted_size);
        int dec_result = decrypt_chacha20(packet->data + AX100_ENC_SIZE, (int)decrypted_size, (uint8_t*)key, (uint8_t*)iv, decrypted);
        if((unsigned)dec_result != decrypted_size){
            P_ERR_STR("Could not decrypt packet");
            free(decrypted);
            csp_buffer_free(packet);
             continue;
        }
        int result = process_packet((unsigned char*)decrypted, decrypted_size);
        if(result == 1){
          memcpy(crap_packet, decrypted, decrypted_size);
          *crap_size = decrypted_size;
          csp_buffer_free(packet);
          free(decrypted);
          break;
        }
        else if (result == 2){
          //Tell libcrap to shut down client
          csp_buffer_free(packet);
          free(decrypted);
          return R_END_TRNS;
        }
        else {
            //Received command grammar packet, fetch next packet
            /* Calling recursively can cause a buffer overflow. Returning to
             * libcrap early can wreak timeout system havoc. Using gotos is a
             * mess. Solution: Use a loop and continue here to get next packet.
             */
            csp_buffer_free(packet);
            free(decrypted);
            continue;
        }
    }
    else {
        P_INFO_STR("csp_crap_recv() timeout");
        return R_TIMEOUT;
    }
    }
    return EXIT_SUCCESS;
}

static unsigned get_iv() {
  //This code is designed to be as error resistant as possible and will fall back to using a simple increasing iv if acquiring iv from disk does not succeed.
  //Due to issues with a previous attempt to implement this feature, the current code includes some redundancies.
  //TODO: Perhaps reserve and get a packet with 100 iv's at once, should keep file writes to a minimum and not cut down the number of usable iv's a lot unless the process is started millions of times
  unsigned iv = 0;
  FILE* fp = fopen(AX100_IV_FILE, "r+");
  if (fp) {
    //Continue even if we have an invalid value for the first four bytes, it will get corrected to AX100_LOWEST_IV. Trailing bytes likewise get ignored.
    fread(&iv, sizeof(unsigned), 1, fp);
    //Ensure we are using a sane iv value: odd and at least AX100_LOWEST_IV.
    iv |= 1;
    iv += 2;
    //This is needed so the ground can be sure that this is an encrypted packet
    if ((iv & 0xff) < AX100_LOWEST_IV) {
        iv &= (~0xffU);
        iv |= AX100_LOWEST_IV;
    }
    rewind(fp);
    fwrite(&iv, sizeof(unsigned), 1, fp);
    fclose(fp);
  }
  else {
    iv = AX100_LOWEST_IV;
    fp = fopen(AX100_IV_FILE, "w");
    if (fp) {
      //Create file if it does not exist, it will be available for the next packet
      fwrite(&iv, sizeof(unsigned), 1, fp);
      fclose(fp);
      //Make sure the file is accessible even if another user will be using the file.
      chmod(AX100_IV_FILE, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    }
    else {
      //Fallback option
      static unsigned static_iv = AX100_LOWEST_IV;
      if ((static_iv & 0xff) < AX100_LOWEST_IV) {
          static_iv &= (~0xffU);
          static_iv |= AX100_LOWEST_IV;
      }
      iv = static_iv;
      static_iv += 2;
    }
  }
  return iv;
}

//Use udp instead of the radio for libcrap communications.
#ifdef UDP_COMMS
#warning "UDP comms are intended for debugging only. Remember to disable before flight."

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

int recv_message_udp(unsigned char* packet, struct timeval* timeout) {
    int recv_sock;
    struct sockaddr_storage recv_addr;
    unsigned recv_addr_len = sizeof(recv_addr);

    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);
    struct addrinfo* recv_info;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

    int error = getaddrinfo(NULL, GR_PORT_STR,  &hints, &recv_info);
    if(error){
        P_ERR("%s", gai_strerror(error));
        return -1;
    }
    if ((recv_sock = socket(recv_info->ai_family, recv_info->ai_socktype, recv_info->ai_protocol)) < 0) {
        P_ERR("socket creation failed: %s", strerror(errno));
        return -1;
    }

    if (bind(recv_sock, recv_info->ai_addr, recv_info->ai_addrlen) < 0) {
      P_ERR("bind() failed: %s", strerror(errno));
      return -1;
    }
    freeaddrinfo(recv_info);


    if (setsockopt(recv_sock, SOL_SOCKET, SO_RCVTIMEO, timeout, sizeof(struct timeval)) < 0) {
        P_ERR("setsockopt: %s", strerror(errno));
    }

    int n = (int)recvfrom(recv_sock, packet, PACKET_BUF_SIZE,
                    0, (struct sockaddr*)&recv_addr,
                    &recv_addr_len);
    close(recv_sock);

    if (n <= 0) {
        P_INFO_STR("Timeout reached.");
        return n;
    }
    return n;
}

#define CLIENT_SERVER_PORT 1237
static int send_udp_packet(const void* packet, size_t length){
  int sock = socket(PF_INET, SOCK_DGRAM, 0);
  if(sock < 0){
    return 1;
  }

  struct sockaddr_in gr_info;
  gr_info.sin_family = AF_INET;

  gr_info.sin_port = htons(CLIENT_SERVER_PORT);
  gr_info.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

  if(sendto(sock, packet, length, 0, (const struct sockaddr *)&gr_info, sizeof(gr_info)) < 0){
    P_ERR("sendto(): %s", strerror(errno));
    return -1;
  }
  close(sock);
  return 0;
}
#endif

int csp_crap_send2(const uint8_t* crap_packet, size_t crap_size, struct timeval* timeout, unsigned char is_encrypted){
    P_INFO("crap_send called with %zu bytes raw size", crap_size);
    (void)timeout;
    if (crap_size > CRAP_MAX_MTU + (!is_encrypted * AX100_ENC_SIZE)) {
        P_ERR_STR("packet too large to send");
        return EXIT_FAILURE;
    }

    int result = 0;
    const unsigned char* send_buffer;
    uint8_t* encrypted;
    if (is_encrypted) {
      size_t encrypted_size = crap_size + AX100_ENC_SIZE;
      encrypted = (uint8_t*)malloc(encrypted_size);
      uint32_t iv[4] = {0};
      iv[0] = get_iv();
      uint64_t key[4] = {0};
      key[0] = AX100_ENC_KEY0;
      key[1] = AX100_ENC_KEY1;
      key[2] = AX100_ENC_KEY2;
      key[3] = AX100_ENC_KEY3;
      result = encrypt_chacha20(crap_packet, (int)crap_size, (uint8_t*)key, (uint8_t*)iv, encrypted + AX100_ENC_SIZE);
      if((unsigned)result != crap_size){
        P_ERR_STR("Could not encrypt data");
        return EXIT_FAILURE;
      }
      memcpy(encrypted, iv, AX100_ENC_SIZE);
      crap_size = encrypted_size;
      send_buffer = encrypted;
    }
    else {
      send_buffer = crap_packet;
    }
#ifdef UDP_COMMS
    result = send_udp_packet(send_buffer, crap_size);
    if (is_encrypted) {
        free(encrypted);
    }
    if(result != CSP_ERR_NONE) {
        P_ERR_STR("Sending via udp failed");
        return EXIT_FAILURE;
    }
#else
    csp_packet_t* packet = csp_buffer_get(crap_size);
    memcpy(packet->data, send_buffer, crap_size);
    if (is_encrypted) {
        free(encrypted);
    }
    packet->length = (uint16_t) crap_size;
    result = csp_sendto(CSP_PRIO_NORM, GROUND_ADDR, GROUND_PORT, CSP_CDH_PORT, CSP_O_NONE, packet, (uint32_t)(1000 * timeout->tv_sec + timeout->tv_usec / 1000));
    if(result != CSP_ERR_NONE) {
        csp_buffer_free(packet);
        P_ERR_STR("Sending to ground failed");
        return EXIT_FAILURE;
    }
#endif
    else {
        //Add protocol overhead size as crap_size is data only
        usleep((uint32_t)((crap_size + AX100_WRAP_SIZE) * 1000 * 1000 / (AX100_BAUD / 8)));
    }
    if (is_encrypted) {
      //If sending a non-encrypted packet – i.e. not a beacon – we are in some kind of transmission and won't need to send beacons for a while
      next_beacon_time = time(0) + HMD_BEACON_SEND_FREQ_TRANSMISSION;
    }
    return EXIT_SUCCESS;
}

int csp_crap_send(const uint8_t* crap_packet, size_t crap_size, struct timeval* timeout) {
  return csp_crap_send2(crap_packet, crap_size, timeout, 1);
}

void csp_crap_received_file(char* file){
  if(!file){
    return;
  }
  size_t len = strnlen(file, CRAP_MAX_PATH_LEN);

  if(len > 6 && memcmp(file + (len - 6), ".sched", 7) == 0){
    //Execute as schedule file
    P_INFO_STR("Executing schedule file");
    pid_t pid = fork();
    if(pid == -1) {
        P_ERR_STR("Failed to create child process for file");
    }
    else if(pid == 0) {
        //Chmod so it is easier to execute and delete the file
        int result = chmod(file, S_IRWXU | S_IRWXG | S_IRWXO);
        if(result == -1) {
            P_ERR_STR("Failed to chmod file");
        }
        char arg_buf[2 * CRAP_MAX_PATH_LEN + 6];
        sprintf(arg_buf, "%s; rm %s", file, file);
        result = execlp("sh", "sh", "-c", arg_buf, (char*)0);
        if(result == -1) {
            P_ERR_STR("Failed to execute file");
            exit(EXIT_FAILURE);
        }
    }
    else {
        signal(SIGCHLD,SIG_IGN);
    }
  }

//Do nothing
}

static int client_one(){
  //create a new session
  crap_client_sess_t sess;

  struct timeval t;
  t.tv_sec = 2;
  t.tv_usec = 0;

//Having 16 2 second timeouts should allow us to send a beacon right after the 30 second interval for no beacons to be sent.
#define CRAP_RGD_TIMEOUTS 16
  if(crap_init_client(&sess, &csp_crap_send, &csp_crap_recv, csp_crap_received_file, t, CRAP_CHUNK_STORE_RGD, 0, CRAP_RGD_TIMEOUTS) != EXIT_SUCCESS){
      P_ERR_STR("crap_init_client failed");
      return 1;
  }
  return 0;
}

static int server(char* filename){
  //create a new session
  crap_server_sess_t sess;

  // this is per packet timeout
  // i.e. if trying to recv a packet, crap_recv will wait this long
  // before terminating
  struct timeval t;
  t.tv_sec = 6;
  t.tv_usec = 0;

  // ideally get rid of this too, library should maintain state
  // definitely fix the directory shit
  int err = crap_init_server(&sess, &csp_crap_recv, &csp_crap_send, t, 1);
  if (err != EXIT_SUCCESS) {
      P_ERR_STR("Server failed to initialize");
      return EXIT_FAILURE;
  }

  // send the file
  err = crap_send_file(&sess, filename);
  if (err != R_SESSION_COMPLETE) {
      P_ERR_STR("crap_send_file failed");
      return EXIT_FAILURE;
  }
  P_INFO_STR("crap_send_file finished");
  return EXIT_SUCCESS;
}

int main(int argc, char* argv[]){
#ifndef UDP_COMMS
    daemonize();
#endif
    int error = sll_init(&commands, 0);
    if(error){
        P_ERR_STR("Could not initialize sll");
        return EXIT_FAILURE;
    }

    //TODO: Useful to have full interface with help, --device
    char* device = "/dev/ttyUSB1";
    device = argv[1];

    csp_socket_t* sock = csp_initalize_socket(device);
    if(!sock){
        P_ERR_STR("Could not initialize sock");
        return EXIT_FAILURE;
    }
    globalsock = sock;
    if(argc > 2){
        //Insert file name.
        //SLL no longer does memory allocation for us and we need to duplicate the string in other locations, so do it here too. This will be free'd at the time of retrieval.
        char* filename = strdup(argv[2]);
        if(sll_push_head(&commands, filename, strnlen(filename, CRAP_MAX_PATH_LEN), SLL_NOWRITE) != EXIT_SUCCESS){
            P_ERR_STR("Could not add file name to sll");
        }
    }

    next_beacon_time = time(0) + HMD_BEACON_SEND_FREQ_IDLE;
    while(true){
#ifndef UDP_COMMS
        if (ax100_check_alive() != EXIT_SUCCESS) {
            //Could try to re-initialize the csp_initalize_socket, but power cycling the radio and restarting the process is easier and should work every time.
            //Power cycle the ax100, then re-init?
            P_ERR_STR("Can't reach the ax100. Power cycling ax100, waiting and restarting the process...");

            //New approach: Use a script that should handle this automatically. Use fork-exec-no wait so that the script not terminating won;t be an issue, then give the script some time to finish.
            pid_t pid = fork();
            if(pid == -1) {
                P_ERR_STR("Failed to create child process for power cycle");
            }
            else if(pid == 0) {
                int result = execlp("sh", "sh", "-c", "/root/scripts/power_cycle_radio.sh", (char*)0);
                if(result == -1) {
                    P_ERR_STR("Failed to execute power cycle script");
                    exit(EXIT_FAILURE);
                }
            }
            else {
                signal(SIGCHLD,SIG_IGN);
            }
            //Give the script time to finish
            sleep(5);
            //TODO: Wait a full 30 seconds here to avoid too frequent power cycling/process restarts?
            sleep(25);

            //Exit the process, hm-d will bring us back, we should be able to establish a proper connection upon restart
            exit(EXIT_FAILURE);

            /*
            //Tell pb-d to turn radio off
            rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_DIS, PBD_ID_HS_CH5_3V3_AM, 10*1000*1000);
            sleep(5);
            //Tell pb-d to turn radio on
            rpc_send_uint32(PBD_RPC_SOCKET, PBD_OP_HS_EN, PBD_ID_HS_CH5_3V3_AM, 10*1000*1000);
            sleep(5);
            */
        }
#endif
        char* filename = sll_pull_tail(&commands);
        //Send a beacon at a set interval if no recent packets sent or received
        time_t curr_time = time(0);
        if (curr_time > next_beacon_time) {
          ax100_send_error(AX100_OP_BEACON, AX100_OP_BEACON);
          next_beacon_time = curr_time + HMD_BEACON_SEND_FREQ_IDLE;
        }
        if(filename) {
          server(filename);
          free(filename);
        }
        else {
          P_INFO_STR("No files in queue, starting client...");
          client_one();
      }
    }

    sll_close(&commands);
    return EXIT_SUCCESS;
}

