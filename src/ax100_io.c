#include "ax100_io.h"

csp_packet_t* recv_message(csp_socket_t* sock, struct timeval* timeout) {
    /* Pointer to current connection and packet */
    csp_conn_t * conn;
    csp_packet_t * packet;

    /* Process incoming connections */

    /* Wait for connection, with given timeout */
    //This will solve the potential problem of several packages given by one 
    //Performance effect of having just one packet per connection is believed to be negligibile.
    if ((conn = csp_accept(sock, (uint32_t)(1000 * timeout->tv_sec + timeout->tv_usec / 1000))) == NULL){
      return NULL;
    }

    /* Read packet(s?) with given timeout */
    while ((packet = csp_read(conn, (uint32_t)(1000 * timeout->tv_sec + timeout->tv_usec / 1000))) != NULL) {
        csp_close(conn);
        return packet;
    }

    /* Close current connection, and handle next */
    csp_close(conn);

    return NULL;

}
